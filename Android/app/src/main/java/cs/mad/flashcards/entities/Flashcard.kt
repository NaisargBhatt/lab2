package cs.mad.flashcards.entities

data class Flashcard(val term: String , val definition: String) {
    val flashcards = arrayOf<Flashcard>(Flashcard("A","letter A"),Flashcard("B","letter B")
                                    ,Flashcard("C","letter C"),Flashcard("D","letter D")
                                    ,Flashcard("E","letter E"), Flashcard("F","letter F")
                                    ,Flashcard("G","letter G"),Flashcard("H","letter H")
                                    ,Flashcard("I","letter I"),Flashcard("J","letter J"))

    fun draw(): Array<Flashcard>{
        return flashcards
    }
}