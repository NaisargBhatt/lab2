package cs.mad.flashcards.entities

data class FlashcardSet (val title: String ) {
    val setOfCards = arrayOf<FlashcardSet>(FlashcardSet("set1"),FlashcardSet("set2"),
                FlashcardSet("set3"),FlashcardSet("set4"), FlashcardSet("set5"),
                FlashcardSet("set6"),FlashcardSet("set7"),FlashcardSet("set8"),
                FlashcardSet("set9"),FlashcardSet("set10"))

    fun draw(): Array<FlashcardSet>{
        return setOfCards
    }
}